﻿using System;
using TechTalk.SpecFlow;

namespace _30._06.Steps
{
    [Binding]
    public class CartSteps
    {
        [Given(@"https/allo website is open")]
        public void GivenHttpsAlloWebsiteIsOpen()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Given(@"User is not logged in")]
        public void GivenUserIsNotLoggedIn()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"User clicks on cart")]
        public void WhenUserClicksOnCart()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"Cart is empty")]
        public void ThenCartIsEmpty()
        {
            ScenarioContext.Current.Pending();
        }
    }
}
